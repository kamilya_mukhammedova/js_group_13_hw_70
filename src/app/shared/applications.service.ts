import { Injectable } from '@angular/core';
import { ApplicationForm } from './application-form.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ApplicationsService {
  userApplicationUploading = new Subject<boolean>();

  private applicationsArray: ApplicationForm[] = [];

  constructor(private http: HttpClient) {}

  addUserApplication(form: ApplicationForm) {
    const body = {
      name: form.name,
      surname: form.surname,
      patronymic: form.patronymic,
      phoneNumber: form.phoneNumber,
      jobOrStudyPlace: form.jobOrStudyPlace,
      skills: form.skills,
      tShirtVariant: form.tShirtVariant,
      size: form.size,
      comment: form.comment,
    };
    this.userApplicationUploading.next(true);
    return this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/form2.json', body)
      .pipe(tap(() => {
        this.userApplicationUploading.next(false);
      }, () => {
        this.userApplicationUploading.next(true);
      }));
  }




}
