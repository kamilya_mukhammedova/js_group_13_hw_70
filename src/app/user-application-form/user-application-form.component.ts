import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneNumberValidator } from './validate-phoneNumber.directive';
import { ApplicationsService } from '../shared/applications.service';
import { Subscription } from 'rxjs';
import { ApplicationForm } from '../shared/application-form.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-application-form',
  templateUrl: './user-application-form.component.html',
  styleUrls: ['./user-application-form.component.css']
})
export class UserApplicationFormComponent implements OnInit, OnDestroy {
  applicationForm!: FormGroup;
  userApplicationUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(private applicationsService: ApplicationsService, private router: Router) { }

  ngOnInit(): void {
    this.applicationForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      patronymic: new FormControl('', [Validators.required, Validators.minLength(2)]),
      phoneNumber: new FormControl('', [Validators.required, phoneNumberValidator]),
      jobOrStudyPlace: new FormControl('', [Validators.required, Validators.minLength(3)]),
      skills: new FormArray([]),
      tShirtVariant: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(300)
      ])
    });

    this.userApplicationUploadingSubscription = this.applicationsService.userApplicationUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.applicationForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  formIsInvalid() {
    return this.applicationForm.invalid;
  }

  onSubmit() {
    const id =  Math.random().toString();
    const userApplication = new ApplicationForm(
      id,
      this.applicationForm.value.name,
      this.applicationForm.value.surname,
      this.applicationForm.value.patronymic,
      this.applicationForm.value.phoneNumber,
      this.applicationForm.value.jobOrStudyPlace,
      this.applicationForm.value.skills,
      this.applicationForm.value.tShirtVariant,
      this.applicationForm.value.size,
      this.applicationForm.value.comment
    )
    this.applicationsService.addUserApplication(userApplication).subscribe(() => {
      void this.router.navigate(['/thanks']);
    });
  }

  addSkill() {
    const skills = <FormArray>this.applicationForm.get('skills');
    const skillsGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      levelOfSkill: new FormControl('', Validators.required)
    });
    skills.push(skillsGroup);
  }

  getSkillControls() {
    const skills = <FormArray>this.applicationForm.get('skills');
    return skills.controls;
  }

  ngOnDestroy(): void {
    this.userApplicationUploadingSubscription.unsubscribe();
  }
}
