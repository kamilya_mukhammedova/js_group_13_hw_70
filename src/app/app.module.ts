import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { UserApplicationFormComponent } from './user-application-form/user-application-form.component';
import { ThanksComponent } from './thanks.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidatePhoneNumberDirective } from './user-application-form/validate-phoneNumber.directive';
import { ApplicationsService } from './shared/applications.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    UserApplicationFormComponent,
    ThanksComponent,
    ValidatePhoneNumberDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ApplicationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
