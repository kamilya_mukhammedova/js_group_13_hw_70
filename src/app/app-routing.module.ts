import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserApplicationFormComponent } from './user-application-form/user-application-form.component';
import { ThanksComponent } from './thanks.component';

const routes: Routes = [
  {path: '', component: UserApplicationFormComponent},
  {path: 'thanks', component: ThanksComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
